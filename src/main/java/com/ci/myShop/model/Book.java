package com.ci.myShop.model;

import java.util.List;

import com.ci.myShop.model.Item;

public class Book extends Item {
	int nbPage;
	String author;
	String publisher;
	int year;
	int age;

	public Book(String name, int id, float price, int nbrElt, int nbPage, String author, String publisher, int year,
			int age) {
		super(name, id, price, nbrElt);
		this.nbPage = nbPage;
		this.author = author;
		this.publisher = publisher;
		this.year = year;
		this.age = age;
	}

	public int getNbPage() {
		return nbPage;
	}

	public void setNbPage(int nbPage) {
		this.nbPage = nbPage;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public String getPublisher() {
		return publisher;
	}

	public void setPublisher(String publisher) {
		this.publisher = publisher;
	}

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public class BookToTouch extends Book {
		String material;
		int durability;

		public BookToTouch(String name, int id, float price, int nbrElt, int nbPage, String author, String publisher,
				int year, int age, String material, int durability) {
			super(name, id, price, nbrElt, nbPage, author, publisher, year, age);
			this.material = material;
			this.durability = durability;
		}

		public String getMaterial() {
			return material;
		}

		public void setMaterial(String material) {
			this.material = material;
		}

		public int getDurability() {
			return durability;
		}

		public void setDurability(int durability) {
			this.durability = durability;
		}
	}

	public class MusicalBook extends Book {
		List<String> listOfSound;
		int lifetime;
		int nbrBattery;

		public MusicalBook(String name, int id, float price, int nbrElt, int nbPage, String author, String publisher,
				int year, int age, List<String> listOfSound, int lifetime, int nbrBattery) {
			super(name, id, price, nbrElt, nbPage, author, publisher, year, age);
			this.listOfSound = listOfSound;
			this.lifetime = lifetime;
			this.nbrBattery = nbrBattery;
		}

		public List<String> getListOfSound() {
			return listOfSound;
		}

		public void setListOfSound(List<String> listOfSound) {
			this.listOfSound = listOfSound;
		}

		public int getLifetime() {
			return lifetime;
		}

		public void setLifetime(int lifetime) {
			this.lifetime = lifetime;
		}

		public int getNbrBattery() {
			return nbrBattery;
		}

		public void setNbrBattery(int nbrBattery) {
			this.nbrBattery = nbrBattery;
		}
	}

	public class PuzzleBook extends Book {
		int nbrPieces;

		public PuzzleBook(String name, int id, float price, int nbrElt, int nbPage, String author, String publisher,
				int year, int age, int nbrPieces) {
			super(name, id, price, nbrElt, nbPage, author, publisher, year, age);
			this.nbrPieces = nbrPieces;
		}

		public int getNbrPieces() {
			return nbrPieces;
		}

		public void setNbrPieces(int nbrPieces) {
			this.nbrPieces = nbrPieces;
		}
	}

	public class OriginalBook extends Book {
		boolean isNumeric;

		public OriginalBook(String name, int id, float price, int nbrElt, int nbPage, String author, String publisher,
				int year, int age, boolean isNumeric) {
			super(name, id, price, nbrElt, nbPage, author, publisher, year, age);
			this.isNumeric = isNumeric;
		}

		public boolean isNumeric() {
			return isNumeric;
		}

		public void setNumeric(boolean isNumeric) {
			this.isNumeric = isNumeric;
		}
	}
}
