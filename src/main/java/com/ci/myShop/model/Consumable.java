package com.ci.myShop.model;

public class Consumable extends Item {
	int quantity;

	public Consumable(String name, int id, float price, int nbrElt, int quantity) {
		super(name, id, price, nbrElt);
		this.quantity = quantity;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public class Paper extends Consumable {
		String quality;
		float weight;

		public Paper(String name, int id, float price, int nbrElt, int quantity, String quality, float weight) {
			super(name, id, price, nbrElt, quantity);
			this.quality = quality;
			this.weight = weight;
		}

		public String getQuality() {
			return quality;
		}

		public void setQuality(String quality) {
			this.quality = quality;
		}

		public float getWeight() {
			return weight;
		}

		public void setWeight(float weight) {
			this.weight = weight;
		}
	}

	public class Pen extends Consumable {
		String color;
		int durability;

		public Pen(String name, int id, float price, int nbrElt, int quantity, String color, int durability) {
			super(name, id, price, nbrElt, quantity);
			this.color = color;
			this.durability = durability;
		}

		public String getColor() {
			return color;
		}

		public void setColor(String color) {
			this.color = color;
		}

		public int getDurability() {
			return durability;
		}

		public void setDurability(int durability) {
			this.durability = durability;
		}
	}
}
