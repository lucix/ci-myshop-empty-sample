package com.ci.myShop;

import java.util.ArrayList;

import com.ci.myShop.controller.Shop;
import com.ci.myShop.controller.Storage;
import com.ci.myShop.model.Item;

public class Launch {

	public static void main(String[] args) {
		ArrayList<Item> items = new ArrayList<>();
		
		Item it1 = new Item("Bible", 1, 7.99f, 3439);
		Item it2 = new Item("Le Petit Prince", 2, 4.99f, 2598);
		Item it3 = new Item("Le Clan des Otori T1", 3, 6.99f, 30);
		Item it4 = new Item("Le Clan des Otori T2", 4, 6.99f, 30);
		
		items.add(it1);
		items.add(it2);
		items.add(it3);
		
		// constructeur variable shop type Shop
		Shop shop = new Shop(items,0);
		
		shop.sell("Bible");
		shop.sell("Le Petit Prince");
		shop.buy(it4);
		
		System.out.println(shop.getCash());
		
	}

}
