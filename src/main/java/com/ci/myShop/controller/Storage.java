package com.ci.myShop.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.ci.myShop.model.Book;
import com.ci.myShop.model.Item;

public class Storage {
	private Map <String, Item> itemMap;
	
	public Map<String, Item> getItemMap() {
		return itemMap;
	}

	public Storage() {
		super();
		this.itemMap=new HashMap<String, Item>();
	}

	public void addItem(Item obj) {
		itemMap.put(obj.getName(), obj);
	}
	
	public Item getItem(String name) {
		Item it = this.itemMap.get(name);
		return it;
	}
	
	public List<Book> getListBook(){
		List<Book> list;
		list=new ArrayList<Book>();
		
		for(Item item:itemMap.values()) {
			if (item instanceof Book) {
				Book currentBook = (Book) item;
				list.add(currentBook);
			}
		}
		return list;
	}
		
}
