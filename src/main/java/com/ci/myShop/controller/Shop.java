package com.ci.myShop.controller;

import java.util.ArrayList;
import java.util.List;

import com.ci.myShop.model.Book;
import com.ci.myShop.model.Consumable;
import com.ci.myShop.model.Item;

public class Shop {
	private Storage storage;
	private float cash;
	
	public float getCash() {
		return cash;
	}
	
	public Storage getStorage() {
		return storage;
	}

	public Item sell(String name) {
        Item item = this.storage.getItem(name);
        if (item == null) {
            return null;
        }
        
        item.setNbrElt(item.getNbrElt()-1);
        cash = cash+item.getPrice(); //équivalent à cash+=item.getPrice()
        
        System.out.println(item.display());
        return item;
	}

	// constructeur du Shop car méthode sans retour et qui porte le même nom que la classe
	public Shop(ArrayList<Item> items, float cashToSet) {
		storage = new Storage();
		cash = cashToSet;
		
		//Ajoute item de ArrayList dans storage jusqu'à ce que fin de liste
		for(int i = 0; i < items.size(); i++) {
			storage.addItem(items.get(i));
		}
		
	}
	
	public boolean buy(Item item) {
		if (cash >= item.getPrice()) {
			cash = cash - item.getPrice();
			item.setNbrElt(item.getNbrElt()+1);
			if (storage.getItem(item.getName()) == null) {
				storage.addItem(item);
			}
			return true;
		}
		else {
			return false;
		}
				 
	}

	public boolean isItemAvailable(String name) {
		Integer quantity = getNbItemInStorage(name);
		if (quantity == 0) {
			return false;
		}
		return true;
	}
	
	public Integer getAgeForBook(String name) {
		Item it = storage.getItem(name);
		
		if (it instanceof Book) {
			Book currentBook = (Book) it;
			int age = currentBook.getAge();
			return age;
		}
		return null;
		
	}
	
	public List<Book>getAllBook() {
		return storage.getListBook();
	}
	
	public Integer getNbItemInStorage(String name) {
		Item item = storage.getItem(name);
		if (item == null) {
			return 0;
		}
		int quantity = item.getNbrElt();
		
		return quantity;
	}
	
	public int getQuantityPerConsumable(String name) {
		//int quantity = storage.getItem(name).getNbrElt();
		Item it = storage.getItem(name);
		if (it instanceof Consumable) {
			Consumable currentConsumable = (Consumable) it;
			int quantity = currentConsumable.getQuantity();
			return quantity;
		}
		return 0;
	}
}
