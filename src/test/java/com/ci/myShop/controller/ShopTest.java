package com.ci.myShop.controller;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Test;

import com.ci.myShop.model.Book;
import com.ci.myShop.model.Consumable;
import com.ci.myShop.model.Item;

public class ShopTest {
	
	@Test
	public void sellItemDoesntExist() {
		//GIVEN
		ArrayList<Item> items = new ArrayList<>();
		Shop shop = new Shop(items,0);
		
		//WHEN
		Item item = shop.sell("Coran");
		
		//THEN
		assertEquals(null,
				item);
	}

	@Test
	public void sellItemExists() {
		//GIVEN
		ArrayList<Item> items = new ArrayList<>();
		Item it1 = new Item("Bible", 1, 7.99f, 3439);
		items.add(it1);
		Shop shop = new Shop(items,0);
		
		//WHEN
		Item item = shop.sell("Bible");
		
		//THEN
		assertEquals(it1,
				item);
		assertEquals(3438,
				it1.getNbrElt());
		assertEquals(7.99f,
				shop.getCash(),
				0.005f); //Delta du float
	}
	
	@Test
	public void buyImpossible() {
		//GIVEN
		ArrayList<Item> items = new ArrayList<>();
		Item it1 = new Item("pencil", 1, 2.99f, 0);
		items.add(it1);
		Shop shop = new Shop(items,0);
		
		//WHEN
		Boolean buyable = shop.buy(it1);
		
		//THEN
		assertEquals(false,
				buyable);
	}
	
	@Test
	public void buyPossible() {
		//GIVEN
		ArrayList<Item> items = new ArrayList<>();
		Item it1 = new Item("pencil", 1, 2.99f, 0);
		items.add(it1);
		Shop shop = new Shop(items, 5);
		
		//WHEN
		Boolean buyable = shop.buy(it1);
		
		//THEN
		assertEquals(true,
				buyable);
		assertEquals(2.01f,
				shop.getCash(),
				0.005f);
		assertEquals(1,
				it1.getNbrElt());	
	}
	
	@Test
	public void buyPossibleAndItemAdded() {
		//GIVEN
		ArrayList<Item> items = new ArrayList<>();
		Item it1 = new Item("pencil", 1, 2.99f, 0);
		Shop shop = new Shop(items, 5);
		Storage storage = shop.getStorage();
		
		//WHEN
		Boolean buyable = shop.buy(it1);
		
		//THEN
		assertEquals(true,
				buyable);
		assertEquals(it1,
				storage.getItem("pencil"));
	}
	
	@Test
	public void isItemAvailableFalse() {
		//GIVEN
		ArrayList<Item> items = new ArrayList<>();
		Item it1 = new Item("pencil", 1, 2.99f, 0);
		items.add(it1);
		Shop shop = new Shop(items,0);
		
		//WHEN
		Boolean available = shop.isItemAvailable("pencil");
		
		//THEN
		assertEquals(false,
				available);
	}
	
	@Test
	public void isItemAvailableTrue() {
		//GIVEN
		ArrayList<Item> items = new ArrayList<>();
		Item it1 = new Item("pencil", 1, 2.99f, 5);
		items.add(it1);
		Shop shop = new Shop(items,0);
		
		//WHEN
		Boolean available = shop.isItemAvailable("pencil");
		
		//THEN
		assertEquals(true,
				available);
	}
	
	@Test
	public void getAgeForBookIfIsABook() {
		//GIVEN
		ArrayList<Item> items = new ArrayList<>();
		Book it1 = new Book("Bible", 1, 7.99f, 2, 3479, "Anonym", "Gulliver editions", 2014, 7);
		items.add(it1);
		Shop shop = new Shop(items,0);
		
		//WHEN
		int age = shop.getAgeForBook("Bible");
		
		//THEN
		assertEquals(7,
				age);
	}
	
	@Test
	public void getAgeForBookIfIsNotABook() {
		//GIVEN
		ArrayList<Item> items = new ArrayList<>();
		Item it1 = new Item("pencil", 1, 7.99f, 2);
		items.add(it1);
		Shop shop = new Shop(items,0);
		
		//WHEN
		Integer age = shop.getAgeForBook("pencil");
		
		//THEN
		assertEquals(null,
				age);
	}
	
	@Test
	public void getNbItemInStorageNameDoesntExist() {
		//GIVEN
		ArrayList<Item> items = new ArrayList<>();
		Shop shop = new Shop(items,0);
		
		//WHEN
		int quantity = shop.getNbItemInStorage("pencil");
		
		//THEN
		assertEquals(0,
				quantity);
	}
	
	@Test
	public void getNbItemInStorageQuantityZero() {
		//GIVEN
		ArrayList<Item> items = new ArrayList<>();
		Item it1 = new Item("pencil", 1, 7.99f, 0);
		items.add(it1);
		Shop shop = new Shop(items,0);
		
		//WHEN
		int quantity = shop.getNbItemInStorage("pencil");
		
		//THEN
		assertEquals(0,
				quantity);
	}
	
	@Test
	public void getQuantityPerConsumableIfConsumable() {
		//GIVEN
		ArrayList<Item> items = new ArrayList<>();
		Consumable it1 = new Consumable("pencil", 1, 7.99f, 5, 2);
		items.add(it1);
		Shop shop = new Shop(items,0);
		
		//WHEN
		int quantity = shop.getQuantityPerConsumable("pencil");
		
		//THEN
		assertEquals(2,
				quantity);
	}
	
	@Test
	public void getQuantityPerConsumableIfNotConsumable() {
		//GIVEN
		ArrayList<Item> items = new ArrayList<>();
		Book it1 = new Book("Le Petit Prince", 1, 5.99f, 5, 50, "Antoine de S.", "Gallimard", 1990, 8);
		items.add(it1);
		Shop shop = new Shop(items,0);
		
		//WHEN
		int quantity = shop.getQuantityPerConsumable("Le Petit Prince");
		
		//THEN
		assertEquals(0,
				quantity);
	}
}
