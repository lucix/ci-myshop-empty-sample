package com.ci.myShop.controller;

import static org.junit.Assert.*;

import java.util.Map;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.ci.myShop.model.Item;

public class StorageTest {
	private Storage storage;
	
	@Before
	public void setUp() {
		storage = new Storage();
	}
	
	@After
	public	void tearDown() {
	}
	
	@Test
	public void addItem() {
		// préparation = construction (GIVEN)
		Item item = new Item("testName", 1, 2.99f, 100);
		
		//ce que je veux tester (WHEN)
		storage.addItem(item);
		
		//les assertions(vérifications) (THEN)
		assertEquals(item, // arg1 = résultat attendu
				storage.getItemMap().get("testName")); // arg2 = résultat réellement obtenu
	}

}
